import string
import os
import sys

def enum(**enums):
    return type('Enum', (), enums)

class Converter:
    def __init__(self):
        self.filename = None
        self.data = None
        self.dataType = None
        self.DataType = enum(ASCII='ASCII', Binary='BINARY', Hex='HEX')
        
    # =================================
    # takes the file that is to be converted
    # =================================
    def newFile(self, filename):
        self.filename = filename
        f = open(self.filename, 'r')
        self.data = f.read()
        f.close()
        #check data type
        self.detectType(self.data)
        
    # =================================
    # detects the type of file's content
    # =================================
    def detectType(self, data):
        self.dataType = self.DataType.ASCII
        #binary detection
        if all((c in set('01')) for c in self.data):
            self.dataType = self.DataType.BINARY
        #hex detection
        elif all((c in string.hexdigits) for c in self.data):
            self.dataType = self.DataType.HEX

    # =================================
    # takes the type to convert to and returns the converted data
    # =================================
    def convertTo(self, filename, convertTo):
        f = open(filename, 'r')
        value = 'invalid parameter'
        ct = convertTo.lower()
        ct = ct.replace(' ', '')
        if ct == 'asciitobinary':
            value = self.asciiToBinary(f)
        elif ct == 'binarytoascii':
            value = self.binToAscii(f)
        elif ct == 'binarytohex':
            value = self.binToHex(f)
        elif ct == 'hextobinary':
            value = self.hexToBin(f)
        elif ct == 'asciitohex':
            value = self.asciiToHex(f)
        elif ct == 'hextoascii':
            value = self.hexToAscii(f)
        f.close()
        return value

    def convert2(self, file, type, count):
        if type == 'ab':
            if count == 'all':
                terminator = ' '
                while terminator != '':
                    terminator = self.asciiToBinary(file)
            else:
                for i in range(0, int(float(count))):
                    self.asciiToBinary(file)
        elif type == 'ba':
            if count == 'all':
                terminator = ' '
                while terminator != '':
                    terminator = self.binToAscii(file)
            else:
                for i in range(0, int(float(count))):
                    self.binToAscii(file)
        elif type == 'ax':
            self.asciiToBinary(file)
        elif type == 'xa':
            self.asciiToBinary(file)
        elif type == 'xb':
            self.asciiToBinary(file)
        elif type == 'bx':
            self.asciiToBinary(file)
        else:
            print "invalid type"
            return 1
        
    # =================================
    # converters
    # =================================
    def asciiToBinary(self, fString):
        value = ''
        #if fString != sys.stdin:
        #    fString.seek(0)
        #while 1:
        char = fString.read(1)
        if not char or char == '\n': 
            return value
        char = ord(char) 
        strg = ''.join('01'[(char >> x) & 1] for x in xrange(7, -1, -1))
        sys.stdout.write(strg)  
        value += str(strg)
        return value

    def binToAscii(self, fString):
        value = ''
        #if fString != sys.stdin:
        #    fString.seek(0)
        try:
        #    while 1:
            char = fString.read(8)
            if not char or char == '\n': 
                return value
            strg = (chr(int(char, 2)))
            sys.stdout.write(strg)
            value += str(strg)  
        except:
            print 'Given input was not proper binary'
            value = 'Given input was not proper binary'
        return value

    def binToHex(self, fString):
        value = ''
        if fString != sys.stdin:
            fString.seek(0)
        try:
            while 1:
                char = fString.read(4)
                if not char or char == '\n': break
                strg = (hex(int(char, 2)))[2:]
                sys.stdout.write(strg)
                value += str(strg)
        except:
            print 'Given input was not proper binary'
            value = 'Given input was not proper binary'
        return value

    def hexToBin(self, fString):
        value = ''
        if fString != sys.stdin:
            fString.seek(0)
        try:
            while 1:
                char = fString.read(1)
                if not char or char == '\n': break
                strg = (bin(int(char, 16)))[2:]
                strg = -len(strg)%4*'0'+strg
                sys.stdout.write(strg)
                value += str(strg)
        except:
            print 'Given input was not proper binary'
            value = 'Given input was not proper binary'
        return value

    def asciiToHex(self, fString):
        value = ''
        if fString != sys.stdin:
           fString.seek(0)
        # convert to bin, store in temp file
        file = "temp"
        tempFile = open(file, 'w+')
        if fString != sys.stdin:
           fString.seek(0)
        while 1:
            char = fString.read(1)
            if not char: break
            char = ord(char) 
            strg = ''.join('01'[(char >> x) & 1] for x in xrange(7, -1, -1))
            tempFile.write(strg)

        tempFile.seek(0)

        # convert to hex
        while 1:
            char = tempFile.read(4)
            if not char: break
            strg = (hex(int(char, 2)))[2:]
            sys.stdout.write(strg)
            value += str(strg)
        os.remove("./temp")
        return value

    def hexToAscii(self, fString):
        value = ''
        # convert hex to bin and store in temp file
        try:
            if fString != sys.stdin:
                fString.seek(0)
                
            file = "temp"
            tempFile = open(file, 'w+')
           
            #for i in range(1, flength):
            #while 1:
            for char in fString.read():
                #char = fString.read(1)
                if char == '\n': break
                strg = (bin(int(char, 16)))[2:]
                strg = -len(strg)%4*'0'+strg
                tempFile.write(strg)

            tempFile.seek(0)

            #convert bin file to ascii
            while 1:
                char = tempFile.read(8)
                if not char: break
                strg = (chr(int(char, 2)))
                sys.stdout.write(strg)
                value += str(strg)
            os.remove("./temp")
        except:
            print 'Given input was not proper hex'
            value = 'Given input was not proper hex'
        return value



